import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  equalOpertor: {
    borderRadius: 50,
    backgroundColor: '#fff',
  },
  operatorDark: {
    backgroundColor: '#363636',
  },
  curvedBtnTop: {
    borderTopEndRadius: 50,
    borderTopLeftRadius: 50,
  },
  curvedBtnBottom: {
    borderBottomEndRadius: 50,
    borderBottomLeftRadius: 50,
  },
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: '#3E403F',
  },

  calculation: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  buttons: {
    flex: 8,
    flexDirection: 'row',
  },
  borderTopStyle: {
    borderBottomColor: '#363636',
    borderBottomWidth: 1,
    marginBottom: 30,
  },

  row: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btn: {
    width: '24%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 10,
  },
  textButton: {
    color: '#7c7c7c',
    fontSize: 28,
    width: '100%',
    height: '100%',
    textAlign: 'center',
    textAlignVertical: 'center',
  },
});

export default styles;
