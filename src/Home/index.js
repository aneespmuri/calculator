import React, {useState} from 'react';
import {Text, View, Pressable} from 'react-native';
import styles from './style';

const App = () => {
  const [currentNumber, setCurrentNumber] = useState('');
  const buttons = [
    'C',
    '+/-',
    '%',
    '/',
    7,
    8,
    9,
    '*',
    4,
    5,
    6,
    '-',
    1,
    2,
    3,
    '+',
    '.',
    0,
    '00',
    '=',
  ];

  const handleSubmit = (data) => {
    if (data === '=') {
      if (currentNumber === '1+3+9') {
        alert('Hello World');
        setCurrentNumber('');
      } else {
        setCurrentNumber('');
      }
    } else {
      setCurrentNumber(currentNumber + data);
    }
  };

  const sideBarOperators = ['/', '*', '-', '+', '='];

  return (
    <View style={styles.container}>
      <View style={styles.calculation}>
        <Text style={{color: '#7c7c7c', fontSize: 28, padding: 20}}>
          {currentNumber}
        </Text>
      </View>
      <View style={styles.borderTopStyle} />
      <View style={styles.buttons}>
        <View style={styles.row}>
          {buttons.map((item) => (
            <Pressable
              onPress={() => handleSubmit(item)}
              key={item}
              style={[
                styles.btn,
                item === '/' && styles.curvedBtnTop,
                item === '=' && styles.curvedBtnBottom,
                sideBarOperators.includes(item) && styles.operatorDark,
              ]}>
              <Text
                style={[
                  styles.textButton,
                  item === '=' && styles.equalOpertor,
                ]}>
                {item}
              </Text>
            </Pressable>
          ))}
        </View>
      </View>
    </View>
  );
};

export default App;
